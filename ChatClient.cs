using System.Net.Sockets;
using System.Text;
using System.Net;
using System;
using System.IO;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using System.Threading;

public delegate void IrcCommandHandler(IrcMessage message);
public delegate void Ui0Callback();
public delegate void CTCPHandler(string sender, string message);

public class ChatClient : Form
{
	ThreadSafeOutput threadSafeOutput = ConsoleOutput.instance;
	public static readonly byte[] CrLf = new byte[] { 13, 10 }; //  represents "\r\n"
	private IrcCommandHandler serverCommandHandler = null;
	private Ui0Callback setDisconnectedControls = null;
	private Ui0Callback setConnectedControls = null;
	private Ui0Callback enableControls = null;
	public const string CTCP_DELIMITER = "\x01";

	//  Declare and initilize UI elements
	private LogBox chatLog = new LogBox(); //  LogBox is my own class that has a method for adding messages and built in thread safety
	private TextBox inputBox = new TextBox();
	private Button sendButton = new Button();
	private TabControl rooms = new TabControl();

	//  Declare colors
	private Color lightColor = Color.FromArgb(225, 225, 235);
	private Color bgColor = Color.FromArgb(255, 255, 255);
	
	private Color faded = Color.FromArgb(75, 75, 75);
	private Color joinColor = Color.Green;
	private Color partColor = Color.DarkRed;
	private Color defaultColor = Color.FromArgb(65, 65, 65);
	private Color userColor = Color.DarkBlue;
	//private Color darkColor = Color.FromArgb(120, 120, 120);

	//  Declare other variables
	private NetworkStream stream = null;
	private Thread networkThread = null;
	private ChatServer server = null;
	private string nick = "";
	private string channel = "";
	private string hostName = "";
	private bool debugMode = false;

	public ChatClient()
	{
		Font = new Font("Courier", 9);
		serverCommandHandler = new IrcCommandHandler(HandleServerCommand);
		setDisconnectedControls = new Ui0Callback(SetDisconnectedControls);
		setConnectedControls = new Ui0Callback(SetConnectedControls);
		enableControls = new Ui0Callback(EnableControls);

		Size = new Size(640, 480);

		//  Set colors
		BackColor = lightColor;
		rooms.BackColor = bgColor;
		//ForeColor = lightColor;
		chatLog.BackColor = lightColor;
		sendButton.BackColor = Color.FromArgb(160, 255, 140);
		sendButton.Font = new Font("Courier", 9, FontStyle.Bold);
		inputBox.AutoSize = false;
		//rooms.BackColor = darkColor;
		//rooms.ForeColor = darkColor;
		//inputBox.BackColor = lightColor;

		//  Set up the send button
		sendButton.Text = "send";
		sendButton.Click += new EventHandler(HandleInput);

		//  Add controls to the form
		SetDisconnectedControls();

		//  Display documentation so user knows how to use the program
		chatLog.AddMessage("use /connect host port nickname");
		chatLog.AddMessage("or /host port nickname");

		//  Redraw the form if the window is resized
		Resize += delegate { DoLayout(); };

		//  Draw the form
		DoLayout();

		//  When the form is shown set the focus to the input box
		Shown += delegate { inputBox.Focus(); };
		rooms.SelectedIndexChanged += delegate { inputBox.Focus(); };

		//  Close the network connection when the form is closed
		//  To prevent any hangups
		FormClosing += delegate { CloseNetwork(); };
	}

	private void SetConnectedControls()
	{
		Controls.Clear();
		Controls.Add(rooms);
		Controls.Add(inputBox);
		Controls.Add(sendButton);

		inputBox.Enabled = false;
		sendButton.Enabled = false;


		DoLayout();
	}

	private void EnableControls()
	{
		inputBox.Enabled = true;
		sendButton.Enabled = true;
		
		inputBox.Focus();
	
		AcceptButton = sendButton;
	}

	private void SetDisconnectedControls()
	{
		Controls.Clear();
		Controls.Add(chatLog);
		Controls.Add(inputBox);
		Controls.Add(sendButton);

		inputBox.Enabled = true;
		sendButton.Enabled = true;

		AcceptButton = sendButton;

		DoLayout();
	}

	//  Just ripped the following 2 methods from dumas's code
	private int GetWidth()
	{
		return (
				Width - (2 * SystemInformation.FrameBorderSize.Width)
		       );
	}

	private int GetHeight()
	{
		return (
				Height - (2 * SystemInformation.FrameBorderSize.Height +
					SystemInformation.CaptionHeight)
		       );
	}

	//  Do the layout
	public void DoLayout()
	{
		chatLog.Width = GetWidth ();
		chatLog.Height = GetHeight() - 25;

		rooms.Width = GetWidth ();
		rooms.Height = GetHeight() - 25;

		inputBox.Top = sendButton.Top = chatLog.Bottom;
		inputBox.Height = sendButton.Height = 25;

		sendButton.Left = inputBox.Width = GetWidth() - 100;
		sendButton.Width = 100;
	}

	// Do this when the user presses send
	public void HandleInput(object sender, EventArgs e)
	{
		// Do nothing if the text is empty
		if (inputBox.Text == "") return;

		threadSafeOutput.WriteLine(inputBox.Text);
		try
		{
			if (inputBox.Text.StartsWith("/")) //  Used if the user wishes to host a chat
			{
				string[] split = inputBox.Text.Split(' ');
				string[] parts = new string[split.Length - 1];

				for (int i = 1; i < split.Length; i++)
					parts[i - 1] = split[i];

				string command = split[0].Substring(1, split[0].Length-1).ToUpper();

				HandleCommand(command, parts);
			}
			else if (hostName != "")
			{
				//  Add message to the users log
				ChannelRoom.SortAddMessage(((ChannelRoom)rooms.SelectedTab).Channel, inputBox.Text, nick, defaultColor, userColor);

				//  If the stream is still connected try send the message
				IrcMessage message = new IrcMessage("PRIVMSG", ((ChannelRoom)rooms.SelectedTab).Channel, inputBox.Text);
				WriteMessage(message);
			}
		}
		catch (Exception e2)
		{
			chatLog.AddMessage(e2.Message, "Error");
			threadSafeOutput.WriteException(e2);
		}	

		inputBox.Text = "";
		inputBox.Focus();
	}

	public void WriteMessage(IrcMessage message)
	{
		Monitor.Enter(this);
		try
		{
			try
			{
				if (stream != null)
				{
					byte[] data = Encoding.UTF8.GetBytes(message.ToString());
					stream.Write(data, 0, data.Length);
				}
			}
			catch (Exception e3)
			{
				threadSafeOutput.WriteException(e3);
				chatLog.AddMessage("Message failed to send");
			}
		}
		finally
		{
			Monitor.Exit(this);
		}
	}
	
	public void ConnectToServer(string host, int port, string pass = "")
	{
		try
		{
			TcpClient client = new TcpClient(host, port); //  Create a new TcpClient and connect to the host
			stream = client.GetStream();        //  Store the stream to the stream variable
			stream.ReadTimeout = 1000;                    //  Set a timeout on the reading so closing doesn't hang up
			ByteBuffer buffer = new ByteBuffer();
			this.Invoke(setConnectedControls); // going to make this happen on a 001 response

			threadSafeOutput.WriteLine("Connecting " + host + " " + port);

			//  Tell the server our nickname
			if (pass != "")
			{
				pass = "PASS " + pass + "\r\n";
			}
			byte[] nickMessage = Encoding.UTF8.GetBytes(pass + "NICK " + nick + "\r\nUSER " + nick + " 0 * :" + nick + "\r\n");
			stream.Write(nickMessage, 0, nickMessage.Length);

			chatLog.AddMessage("connected");

			try
			{
				//  While the stream is connected read for messages
				do
				{
					if(stream != null)
					{
						int delimPos = -1;
						byte[] data = new byte[512];

						//  read until \r\n is found
						do 
						{
							//  While there is no incoming data tell the thread to sleep
							while(!stream.DataAvailable)
							{
								Thread.Sleep(100);
							}
							int length = stream.Read(data, 0, 512);
							buffer.Add(data, length);
							delimPos = buffer.Find(CrLf);
						}
						while(delimPos == -1);

						//  While \r\n can be found in the read data output messages
						do
						{
							byte[] prefix = buffer.ExtractPrefix(delimPos);
							IrcMessage message = IrcMessage.Parse(prefix);

							this.Invoke(serverCommandHandler, message);

							buffer.ExtractPrefix(2); // remove the delimiter
							delimPos = buffer.Find(CrLf);
						}
						while(delimPos != -1);
					}
				} while (stream != null);
			}
			catch(Exception e2)
			{
				if (!(e2 is ThreadAbortException || e2.InnerException is ThreadAbortException || e2.ToString().Contains("Thread")))
					threadSafeOutput.WriteException(e2);
			}

			//  close the connection to the server
			if(client != null)
			{
				client.Close();
				client = null;
			}

			chatLog.AddMessage("disconnected from server");
			networkThread = null;
		}
		catch (Exception e)
		{
			chatLog.AddMessage("Could not connect to server: " + e.Message);
			threadSafeOutput.WriteException(e);
		}
	}

	//  This message is pretty self explanitory
	private void CloseNetwork()
	{
		IrcMessage message = new IrcMessage("QUIT", "Connection closed");
		WriteMessage(message);

		foreach (TabPage room in rooms.TabPages)
		{
			((ChannelRoom)room).Part();
		}
		rooms.TabPages.Clear();
		
		if (this.InvokeRequired)
			this.Invoke(setDisconnectedControls);
		else
			SetDisconnectedControls();

		if (networkThread != null)
		{	
			Thread threadToAbort = networkThread;
			threadToAbort.Abort();
			threadToAbort.Join(1000);
			networkThread = null;
		}

		if (server != null)
		{
			server.Stop();
			server = null;
		}
	}

	public void HandleCommand(string command, string[] parts)
	{
		IrcMessage message;
		switch (command)
		{
			case "HOST":
				//  Close any existing connections
				CloseNetwork();

				threadSafeOutput.WriteLine("starting server");
				this.nick = parts[1];                      //  Set the nickname
				hostName = "server";
				rooms.TabPages.Add(new ChannelRoom(hostName, lightColor));

				//  Start a server in the background
				server = new ChatServer(Convert.ToInt32(parts[0]));
				server.Start();

				//  Start the connection on a thread to connect to the just started server.
				networkThread = new Thread(new ThreadStart(delegate { ConnectToServer("localhost", Convert.ToInt32(parts[0])); }));
				networkThread.Start();
				break;

			case "CONNECT":
				//  Close any existing connections
				CloseNetwork();

				threadSafeOutput.WriteLine("connecting to server");
				this.nick = parts[2];			  //  Set the nickname
				hostName = parts[0];
				rooms.TabPages.Add(new ChannelRoom(hostName, lightColor));

				// Start the connection on a thread to connect to the specified server
				if (parts.Length < 4)
				{
					networkThread = new Thread(new ThreadStart(delegate { ConnectToServer(parts[0], Convert.ToInt32(parts[1])); }));
				} else if (parts.Length == 4)
				{
					networkThread = new Thread(new ThreadStart(delegate { ConnectToServer(parts[0], Convert.ToInt32(parts[1]), parts[3]); }));
				}
				networkThread.Start();
				break;
				
			case "DEBUG":
				chatLog.AddMessage("toggling debug mode");
				debugMode = !debugMode;
				break;
		}

		if (hostName == "")
			return;

		switch (command)
		{
			case "MSG":
				if (parts.Length > 0 && parts[0] != "") 
				{
					bool exists = false;
					foreach (ChannelRoom room in rooms.TabPages)
					{
						if (room.Channel == parts[0]) exists = true;
					}
					
					if (!exists)
					{
						rooms.TabPages.Add(new ChannelRoom(parts[0], lightColor));
						rooms.SelectedIndex = rooms.TabPages.Count - 1;
						//rooms.SelectedIndex = rooms.TabPages.Count - 1;
					}
					string s = "";
					for (int i = 1; i < parts.Length; i++)
					{
						s += parts[i];
						if (i < parts.Length-1) s += " ";
					}
					ChannelRoom.SortAddMessage(parts[0], s, nick, defaultColor, userColor);
					message = new IrcMessage("PRIVMSG", parts[0], s);
					threadSafeOutput.WriteLine(message.ToString());
					WriteMessage(message);
				}
				
				break;
				
			case "RAW":
				string ircCommand = parts[0];
				List<string> parameters = new List<string>(parts);
				parameters.RemoveAt(0);
				WriteMessage(new IrcMessage(ircCommand, parameters.ToArray()));
				break;
				
			case "DISCONNECT":
				//  Close any existing connections
				CloseNetwork();
				chatLog.AddMessage("disconnected");
				this.nick = null;
				hostName = "";
				break;
				
			case "QUERY":
				if (parts[0] != "") {
					rooms.TabPages.Add(new ChannelRoom(parts[0], lightColor));
					rooms.SelectedIndex = rooms.TabPages.Count - 1;
				}
				break;
				
			case "JOIN":
				if ((parts[0].StartsWith("#") || parts[0].StartsWith("&")) && channel == "")
				{
					message = new IrcMessage(command, parts[0]); 
					WriteMessage(message);
				}
				break;
			case "NICK":
				if (parts.Length > 0)
				{
					message = new IrcMessage(command, parts[0]); 
					WriteMessage(message);
				}
				break;

			case "LEAVE":
			case "PART":
				inputBox.Text = "";
				inputBox.Focus();

				if (parts.Length == 0)
					parts = new string[] { ((ChannelRoom)rooms.SelectedTab).Channel };

				if (rooms.TabPages.Count < 2) return;
				for (int i = 1; i < rooms.TabPages.Count; i++)
				{
					if (((ChannelRoom)rooms.TabPages[i]).Channel == parts[0])
					{
						rooms.SelectedIndex = 0;
						((ChannelRoom)rooms.TabPages[i]).Part();
						rooms.TabPages.RemoveAt(i);
						if (!parts[0].StartsWith("#"))
							return;
						
						message = new IrcMessage(command, parts[0]);
						WriteMessage(message);
						return;
					}

				}
				break;

			case "ME":
				string m = inputBox.Text.Split(new char[] { ' ' }, 2)[1];

				//  Add message to the users log
				ChannelRoom.SortAddMessage(((ChannelRoom)rooms.SelectedTab).Channel, "* " +nick + " " + m);

				//  If the stream is still connected try send the message
				message = new IrcMessage("PRIVMSG", ((ChannelRoom)rooms.SelectedTab).Channel, CTCP_DELIMITER + "ACTION " + m + CTCP_DELIMITER);
				WriteMessage(message);
				break;
		}
	}

	public void HandleServerCommand(IrcMessage message)
	{
		string shortPrefix = "";
		if (message.Prefix != null)
			shortPrefix = message.Prefix.Split('!')[0];

		bool toRoom = false;
		int index = 0;

		for (index = 0; index < message.Parameters.Length; index++)
		{
			if (message.Parameters[index].StartsWith("#"))
			{
				toRoom = true;
				break;
			}
		}

		string to = (toRoom) ? message.Parameters[index] : hostName;
		IrcMessage reply;

		switch (message.Command)
		{
			case "001":
				Invoke(enableControls);
				// SetConnectedControls();
				break;
			
			case "353":
				ChannelRoom.SortUpdateNames(to, message.Parameters[message.Parameters.Length - 1]);
				break;
				
			case "366": 
				ChannelRoom.SortUpdateNames(to, "");
				break;
			case "401":
				ChannelRoom.SortAddMessage(message.Parameters[message.Parameters.Length-2], message.Parameters[message.Parameters.Length-1], faded);
				break;
				
			case "MODE":
				
				if (message.Parameters.Length < 2) 
				{
					threadSafeOutput.WriteLine("Too Short");
					break;
				}
				if (message.Parameters[0].StartsWith("#"))
				{
					switch (message.Parameters[1])
					{
						case "+o":
							if (message.Parameters.Length > 2)
							{
								//ChannelRoom.SortAddMessage(to, message.Parameters[2]);
								ChannelRoom.SortUpdateNames(to, "@" + message.Parameters[2]);
								ChannelRoom.SortUpdateNames(to, "");
							}
							break;
						case "-o":
							if (message.Parameters.Length > 2)
							{
								//ChannelRoom.SortAddMessage(to, message.Parameters[2]);
								ChannelRoom.SortUpdateNames(to, message.Parameters[2]);
								ChannelRoom.SortUpdateNames(to, "");
							}
							break;
						default:
							ChannelRoom.SortAddMessage(to, message.ToString(), defaultColor);
							break;
					}
				}
				//ChannelRoom.SortUpdateNames(to, shortPrefix);
				break;

			case "PRIVMSG":
			case "NOTICE":
				string m = message.Parameters[1];

				if (m.StartsWith(CTCP_DELIMITER) && m.EndsWith(CTCP_DELIMITER))
				{
					m = m.Substring(1, m.Length - 2);
					string[] parts = m.Split(new char[] { ' ' }, 2);
					DoCTCP(shortPrefix, parts[0], to, parts[1]);
				}
				else
				{
					if (!to.StartsWith("#")/* && message.Command == "PRIVMSG"*/)
					{
						bool exists = false;
						foreach (ChannelRoom room in rooms.TabPages)
						{
							if (room.Channel == shortPrefix) exists = true;
						}
						
						if (!exists)
						{
							rooms.TabPages.Add(new ChannelRoom(shortPrefix, lightColor));
							//rooms.SelectedIndex = rooms.TabPages.Count - 1;
						}
						
						ChannelRoom.SortAddMessage(shortPrefix, m, shortPrefix, defaultColor);
						break;
					}
					ChannelRoom.SortAddMessage(to, m, shortPrefix, defaultColor);
				}
				break;

			case "JOIN":
				if (shortPrefix == nick)
				{
					rooms.TabPages.Add(new ChannelRoom(to, lightColor));
					rooms.SelectedIndex = rooms.TabPages.Count - 1;
					break;
				}
				ChannelRoom.SortAddMessage(to, "* " + shortPrefix + " has joined the room.", joinColor);
				ChannelRoom.SortUpdateNames(to, shortPrefix);
				break;

			case "PART":
				ChannelRoom.SortAddMessage(to, "* " + shortPrefix + " has left the room.", partColor);
				ChannelRoom.SortRemoveName(to, shortPrefix);
				break;
				
			case "QUIT":
				ChannelRoom.SortQuit(shortPrefix, message.Parameters[0]);
				break;
				
			case "NICK":
				if (shortPrefix == nick)
				{
					nick = message.Parameters[0];
				}
				ChannelRoom.SortNickChange(shortPrefix, message.Parameters[0]);
				break;

			case "PING":
				//threadSafeOutput.WriteLine("ping");
				reply = new IrcMessage("PONG", message.Parameters);
				WriteMessage(reply);
				//threadSafeOutput.WriteLine("pong");
				break;

			default:
				if (debugMode)
				{
					ChannelRoom.SortAddMessage(to, message.ToString(), defaultColor);
				}
				else
				{
					ChannelRoom.SortAddMessage(to, message.Parameters[message.Parameters.Length - 1], defaultColor);
				}
				threadSafeOutput.WriteLine(message);
				break;
		}
	}

	public void DoCTCP(string prefix, string command, string to, string message)
	{
		command = command.ToUpper();
		switch (command)
		{
			case "ACTION":
				ChannelRoom.SortAddMessage(to, "* " + prefix + " " + message, faded);
				break;
			default:
				threadSafeOutput.WriteLine("Unregistered CTCP command: " + command + " from: " + prefix + " to: " + to + "\n" + message);
				break;
		}
	}
}
