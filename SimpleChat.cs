using System;
using System.Text;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Runtime.InteropServices;

public class SimpleChat
{
	[DllImport("kernel32.dll")]
	static extern IntPtr GetConsoleWindow();

	[DllImport("user32.dll")]
	static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

	const int SW_HIDE = 0;
	const int SW_SHOW = 5;

	public static void StartServer(int port)
	{


		//  Create a server on the specified port and
		//  add the cgi XyzDoGet to the extension "xyz"
		ChatServer server = new ChatServer(port);

		//  Start the server
		if(server.Start())
		{
			ConsoleOutput.instance.WriteLine("Ok\n");
		}
	}
	
	public static void Main(string[] args)
	{
		try
		{
			var handle = GetConsoleWindow();
			ShowWindow(handle, SW_HIDE);
		}
		catch (Exception)
		{
			
		}
		//  Default port is set to 6667
		int    port   = 6667;

		for (int i = 0; i < args.Length; i++)
		{
			if (args[i] == "-s")
			{
				//  If -s is found in the arguments check if the port is specified
				for (int j = 0; j < args.Length; j++)
				{
					if (args[j] == "-p" && j < args.Length - 1)
					{
						//  If -p is found set port to the next argument
						port = Convert.ToInt32(args[j+1]);
					}
				}
				//  Start server and return from Main() before client is started
				StartServer(port); 
				return;
			}
		}
		
		
		//  Start the client if -s was not found
		Application.Run(new ChatClient());
	}
}
