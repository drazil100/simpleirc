using System.Net.Sockets;
using System.Text;
using System.Net;
using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;

// a data structure that keeps track of users
public class ChatUser
{
	static private Dictionary<string, ChatUser> byNick = new Dictionary<string, ChatUser>();

	//  get a user by nickname
	static public ChatUser ByNick(string nick)
	{
		Monitor.Enter(byNick);
		ChatUser user = null;
		try
		{
			user = byNick[nick];
		}
		catch(Exception)
		{
			user = null;
		}
		finally
		{
			Monitor.Exit(byNick);
		}
		return user;
	}

	//  variables and properties
	private NetworkStream stream;
	private string nick;
	public string Nick 
	{
		get
		{
			return nick;
		}
		set
		{
			Monitor.Enter(byNick);
			try
			{
				if (nick != null)
				{
					byNick.Remove(nick);
				}
				nick = value;
				byNick.Add(value, this);
			}
			finally
			{
				Monitor.Exit(byNick);
			}
		}
	}

	public ChatUser(NetworkStream stream)
	{
		this.stream = stream;
	}

	//  Send message to the user.
	public void SendMessage(IrcMessage message)
	{
		Monitor.Enter(this);
		try
		{
			byte[] data = Encoding.UTF8.GetBytes(message.ToString());
			if (stream != null)
				stream.Write(data, 0, data.Length);
		}
		finally
		{
			Monitor.Exit(this);
		}
	}

	public void Quit()
	{
		if (nick == null)
			return;

		Monitor.Enter(byNick);
		try
		{
			byNick.Remove(nick);
		}
		finally
		{
			Monitor.Exit(byNick);
		}
	}
}


