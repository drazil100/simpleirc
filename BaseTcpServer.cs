using System.Net.Sockets;
using System.Text;
using System.Net;
using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;

public abstract class BaseTcpServer
{
	protected Thread serverThread = null;                      //  The server's thread
	protected List<Thread> clientThreads = new List<Thread>(); //  List of threads used for the client
	protected TcpListener listener = null;

	//  Since Threading has been introduced I use my own output class
	//  that adds a layer of protection that prevents more than one
	//  thread from performing output at the same time.
	//
	//  ConsoleOutput inherits from ThreadSafeOutput and overrides the
	//  protected abstract WriteData() method with a Console.Write() call
	//  So that Write() and WriteLine() calls can output to the console.
	//
	//  Also by storing this to an object future classes that want to inherit
	//  from BaseTcpServer may set a different ThreadSafeOutput object and
	//  output to other locations like gui or files without worry of threads
	//  interfering with one another or risk of crashing the program
	protected ThreadSafeOutput threadSafeOutput = ConsoleOutput.instance;

	public BaseTcpServer(int port = 80)
	{
		listener = new TcpListener(IPAddress.Any, port);
	}

	//  Start the server on a new thread
	public bool Start()
	{
		try
		{
			listener.Start();
			serverThread = new Thread(new ThreadStart(DoServer));
			serverThread.Start();
		}
		catch(Exception e)
		{
			//  Output any errors current method of output
			threadSafeOutput.WriteException(e);
			return false;
		}
		return true;
	}

	//  Stop the thread the listener is run on and wait for it to close
	//  And then stop the listner
	public void Stop()
	{
		/*
		threadSafeOutput.WriteLine("Stoping threads");
		foreach (Thread t in threadsToStop)
		{
			t.Abort();
			t.Join();
			threadSafeOutput.WriteLine("Thread stopped");
		}
		*/
		threadSafeOutput.WriteLine("Closing server");
		serverThread.Abort();
		serverThread.Join();
		threadSafeOutput.WriteLine("Server closed");

		if (listener != null)
			listener.Stop();
	}

	//  Since this server class is abstract we don't know what kind of
	//  data will be transmitted between the client and server so declare
	//  this method with no implimentation
	protected abstract void HandleClient(TcpClient client);

	
	private void RunClient(TcpClient client, Thread thread)
	{
		try
		{
			//  Call the method that handles client communication
			HandleClient(client);
		}
		finally
		{
			//  When the server is done handling the client (even if it throws an exception)
			//  it removes the thread from the list of client threads
			Monitor.Enter(clientThreads);
			try
			{
				clientThreads.Remove(thread); 
			}
			finally
			{
				Monitor.Exit(clientThreads);
			}
		}
	}

	//  When Start() is called this method will be called on its own thread
	//  and will run presently forever (may change that in the future)
	protected virtual void DoServer()
	{
		TcpClient client = null;

		//  Continuously look for incoming connections and when a connection
		//  is found pass it off to whatever implimentation the class inheriting
		//  from this class has for HandleClient() and do so on a seperate thread
		while (true) 
		{
			try 
			{
				while (!listener.Pending())
				{
					Thread.Sleep(500);
				}

				client = listener.AcceptTcpClient();

				//  Create a new thread and have it run the client.
				Thread t = null;
				t = new Thread(new ThreadStart(delegate { RunClient(client, t); })); 

				//  Add the thread to the list of client threads
				Monitor.Enter(clientThreads);
				try
				{
					clientThreads.Add(t);
				}
				finally
				{
					Monitor.Exit(clientThreads);
				}
				
				//  Run the thread.
				//  Note: The code to remove the thread from the list contained in RunClient
				//  is not run until the thread is started
				t.Start();
			}
			catch (Exception e)
			{
				//  Close threads in case of exception involving threads
				if (e is ThreadAbortException || e.InnerException is ThreadAbortException || e.Message.Contains("Thread"))
				{
					threadSafeOutput.WriteLine("Stoping threads");
					List<Thread> threadsToStop = null;
					Monitor.Enter(clientThreads);
					try
					{
						threadsToStop = new List<Thread>(clientThreads);
						clientThreads.Clear();
					}
					finally
					{
						Monitor.Exit(clientThreads);
					}

					foreach(Thread thread in threadsToStop)
					{
						try
						{
							thread.Abort();
							thread.Join();
						}
						catch(Exception)
						{
							// Ignore exceptions when shutting down
						}
					}
					return;
				}
				else
				{
					threadSafeOutput.WriteException(e);
				}
			}
		}
	}
}
