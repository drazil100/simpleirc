using System;

public class ConsoleOutput : ThreadSafeOutput
{
	//  There is only one console so make sure only this class can create
	//  an instance of it class and declare an instance to a static variable
	private ConsoleOutput() {}
	public readonly static ConsoleOutput instance = new ConsoleOutput();

	//  WriteData() is the method that Write() and WriteLine() call to perform
	//  any form of writing. Write() and WriteLine() both implement thread safety
	//  precautions so that threads will not have any conflict if they both
	//  try to output at the same time
	protected override void WriteData(string output)
	{
		Console.Write(output);
	}
}	
