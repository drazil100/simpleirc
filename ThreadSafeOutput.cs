//  Work in progress but so far the methods I need work
//  and are thread safe
using System;
using System.Collections.Generic;
using System.Threading;

public delegate void OutputMethod(object output);

class OutputRegistry
{
	private event OutputMethod onWriteAll;
	private event OutputMethod onWriteLineAll;

	private readonly static OutputRegistry instance = new OutputRegistry();

	public static void WriteAll(string output)
	{
		Monitor.Enter(instance);
		try
		{
			if (instance.onWriteAll != null)
				instance.onWriteAll(output);
		}
		finally
		{
			Monitor.Exit(instance);
		}
	}

	public static void WriteLineAll(string output)
	{
		Monitor.Enter(instance);
		try
		{
			if (instance.onWriteLineAll != null)
				instance.onWriteLineAll(output);
		}
		finally
		{
			Monitor.Exit(instance);
		}
	}

	public static void Register(OutputMethod target, OutputMethod target2)
	{
		Monitor.Enter(instance);
		try
		{
			instance.onWriteAll += target;
			instance.onWriteLineAll += target2;
		}
		finally
		{
			Monitor.Exit(instance);
		}
	}

	public static void Unregister(OutputMethod target, OutputMethod target2)
	{
		Monitor.Enter(instance);
		try
		{
			instance.onWriteAll -= target;
			instance.onWriteLineAll -= target2;
		}
		finally
		{
			Monitor.Exit(instance);
		}
	}
}

public abstract class ThreadSafeOutput
{
	~ThreadSafeOutput()
	{
		RemoveFromWriteAll();
	}

	public static void WriteAll(object output)
	{
		try
		{
			OutputRegistry.WriteAll(output.ToString());
		}
		catch (Exception e)
		{
			OutputRegistry.WriteAll(e.Message);
		}
	}

	public static void WriteLineAll(object output)
	{
		try
		{
			OutputRegistry.WriteLineAll(output.ToString());
		}
		catch (Exception e)
		{
			OutputRegistry.WriteLineAll(e.Message);
		}
	}


	protected abstract void WriteData(string output);

	public void Write(object output)
	{
		Monitor.Enter(this);
		try
		{
			try
			{
				WriteData(output.ToString());
			}
			catch (Exception e)
			{
				WriteData(e.Message);
			}
		}
		finally
		{
			Monitor.Exit(this);
		}
	}

	public void WriteLine(object output)
	{
		Write(String.Format("{0}\n", output));
	}

	public void WriteException(Exception e)
	{
		Write(String.Format("{0}\n{1}\n", e.Message, e.StackTrace));
	}

	public void AddToWriteAll()
	{
		OutputRegistry.Register(Write, WriteLine);
	}

	public void RemoveFromWriteAll()
	{
		OutputRegistry.Unregister(Write, WriteLine);
	}
}
