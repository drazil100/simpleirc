using System.Net.Sockets;
using System.Text;
using System.Net;
using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;

public class ChatServer : BaseTcpServer
{
	public static readonly byte[] CrLf = new byte[] { 13, 10 }; //  represents "\r\n"
	public Dictionary<string, List<ChatUser>> rooms = new Dictionary<string, List<ChatUser>>();

	//  Pass the port number to the base class
	public ChatServer(int port) : base(port)
	{
		rooms.Add("#default", new List<ChatUser>());
	}

	//  Provide the base class an implimentation to the abstract
	//  HandleClient() method so that it knows what to do when a 
	//  client connects to the server
	protected override void HandleClient(TcpClient client)
	{
		NetworkStream stream = null;  //  The stream of the between server and client
		ByteBuffer buffer = new ByteBuffer();
		ChatUser user = null;

		try
		{

			stream = client.GetStream();
			stream.ReadTimeout = 1000;
			user = new ChatUser(stream);

			//  While the stream is still active
			do
			{
				if(stream != null)
				{
					int delimPos = -1;
					byte[] data = new byte[512];
					do 
					{
						while(!stream.DataAvailable)
						{
							Thread.Sleep(100);
						}
						int length = stream.Read(data, 0, 512);
						buffer.Add(data, length);
						delimPos = buffer.Find(CrLf);
					}
					while(delimPos == -1);

					do
					{
						byte[] prefix = buffer.ExtractPrefix(delimPos);
						IrcMessage message = IrcMessage.Parse(prefix);

						HandleMessage(user, message);

						buffer.ExtractPrefix(2); // remove the delimiter
						delimPos = buffer.Find(CrLf);
					}
					while(delimPos != -1);
				}
			} while (stream != null);
		}
		catch(Exception e)
		{
			if (!(e is ThreadAbortException || e.InnerException is ThreadAbortException || e.ToString().Contains("Thread")))
				Console.Write(e);
		}

		if (user != null)
		{
			foreach(List<ChatUser> room in rooms.Values)
			{
				room.Remove(user);
			}
			user.Quit();
		}

		//  close the connection to the client
		if(client != null)
		{
			client.Close();
			client = null;
		}
	}

	private bool HandleMessage(ChatUser user, IrcMessage message)
	{
		if (user.Nick != null)
		{
			Console.Write(user.Nick + "> ");
		}
		else
		{
			Console.Write("new connection> ");
		}
		Console.Write(message);

		message.Prefix = user.Nick;

		switch (message.Command)
		{
			case "NICK":
				bool firstNick = (user.Nick == null);
				user.Nick = message.Parameters[0];
				if (firstNick)
				{
					user.SendMessage(new IrcMessage("server", "001", new string[] { user.Nick, "Welcome" }));
					user.SendMessage(new IrcMessage("server", "002", new string[] { user.Nick, "Your host is coded-dragon.com" }));
					user.SendMessage(new IrcMessage("server", "003", new string[] { user.Nick, "This server is under construction" }));
					user.SendMessage(new IrcMessage("server", "004", new string[] { user.Nick, "coded-dragon.com", "custom" }));
					rooms["#default"].Add(user);
					List<string> names = new List<string>();
					names.Add(user.Nick);
					names.Add("=");
					names.Add("#default");
					foreach(ChatUser u in rooms["#default"])
					{
						u.SendMessage(new IrcMessage(user.Nick, "JOIN", new string[] { "#default" }));
						names.Add(u.Nick);
					}

					user.SendMessage(new IrcMessage("server", "353", names.ToArray()));
					user.SendMessage(new IrcMessage("server", "366", new string[] { user.Nick, "#default", "end of /NAMES list" }));
				}
				break;

			case "QUIT":
				foreach(ChatUser u in rooms["#default"])
				{
					u.SendMessage(new IrcMessage(user.Nick, "QUIT", new string[] { "client quit" }));
				}
				user.SendMessage(new IrcMessage("ERROR", new string[] { "Closing link" }));
				foreach(List<ChatUser> room in rooms.Values)
				{
					room.Remove(user);
				}
				user.Quit();
				break;

			case "PING":
				user.SendMessage(new IrcMessage("server", "PONG", message.Parameters));
				break;

			case "PRIVMSG":
				if (message.Parameters[0][0] == '#') 
				{
					// send to room
					if (rooms.ContainsKey(message.Parameters[0]))
					{
						foreach(ChatUser target in rooms[message.Parameters[0]])
						{
							if (target != user)
								target.SendMessage(message);
						}
					}
					else
					{
						// send room not found error
					}
				}
				else
				{
					// send to user
					ChatUser target = ChatUser.ByNick(message.Parameters[0]);
					if (target != null)
					{
						target.SendMessage(message);
					}
					else
					{
						// send nick not found error
					}
				}
				break;
		}
		return true;
	}

	/*
	static void Main()
	{
		ChatServer server = new ChatServer(6667);
		server.Start();
	}
	*/
}
