using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

public delegate void AddNamesCallback(string names);
public delegate void RemoveNameCallback(string name);
public delegate void ChangeNameCallback(string oldName, string newName);
public delegate bool ContainsNameCallback(string name);
public delegate bool IsOPCallback(string name);

/*public class UserName
{
	public string name = "";
	public double time = 0;
}*/

public class NameBank : Dictionary<string, List<string>>
{
	ThreadSafeOutput threadSafeOutput = ConsoleOutput.instance;
	public NameBank():base()
	{
		Add("@", new List<string>());
		Add("%", new List<string>());
		Add("+", new List<string>());
		Add("", new List<string>());
	}
	private NameBank(int capacity):base(capacity){}
	
	public void Add(string name, string group = "")
	{
		
		foreach (KeyValuePair<string, List<string>> pair in this)
		{
			if (group != pair.Key && pair.Value.Contains(name))
			{
				for (int i = pair.Value.Count - 1; i >= 0; i--)
				{
					if (pair.Value[i] == name) pair.Value.RemoveAt(i);
				}
			}
		}
		
		//threadSafeOutput.WriteLine(string.Format("name: {0}, group, {1}, contains: {2}", name, group, this.ContainsKey(group)));
		if (!this.ContainsKey(group)) return;
		if (!this[group].Contains(name)) this[group].Add(name);
	}
	
	public void RemoveName(string name)
	{
		foreach (KeyValuePair<string, List<string>> pair in this)
		{
			if (pair.Value.Contains(name))
			{
				for (int i = pair.Value.Count - 1; i >= 0; i--)
				{
					if (pair.Value[i] == name) pair.Value.RemoveAt(i);
				}
			}
		}
	}
	
	public void ChangeName(string oldName, string newName)
	{
		foreach (KeyValuePair<string, List<string>> pair in this)
		{
			if (pair.Value.Contains(oldName))
			{
				for (int i = pair.Value.Count - 1; i >= 0; i--)
				{
					if (pair.Value[i] == oldName) 
					{
						pair.Value.RemoveAt(i);
					}
				}
				pair.Value.Add(newName);
			}
		}
	}
	
	public bool ContainsName(string name)
	{
		bool c = false;
		foreach (KeyValuePair<string, List<string>> pair in this)
		{
			if (pair.Value.Contains(name))
			{
				c = true;
			}
		}
		
		return c;
	}
	
	public bool IsOP(string name)
	{
		return this["@"].Contains(name);
	}
	
	public void SortNames()
	{
		foreach (KeyValuePair<string, List<string>> pair in this)
		{
			pair.Value.Sort();
		}
	}
	
	public override string ToString()
	{
		string newText = "";
		int count = 0;
		
		foreach (string name in this["@"])
		{
			count++;
			newText += " @" + name + "\r\n";
		}
		if (this["@"].Count > 0) newText += "______________\r\n";
		foreach (string name in this["%"])
		{
			count++;
			newText += " %" + name + "\r\n";
		}
		if (this["%"].Count > 0) newText += "______________\r\n";
		foreach (string name in this["+"])
		{
			count++;
			newText += " +" + name + "\r\n";
		}
		if (this["+"].Count > 0) newText += "______________\r\n";
		foreach (string name in this[""])
		{
			count++;
			newText += " " + name + "\r\n";
		}
		
		return string.Format("==============\r\n {0} users\r\n==============\r\n{1}", count, newText);
	}
}

public class NamesList : RichTextBox
{
	ThreadSafeOutput threadSafeOutput = ConsoleOutput.instance;
	private NameBank bank = new NameBank();
	//private string text = "";
	
	public NamesList ()
	{
		ReadOnly = true;
		//LinkClicked += Link_Clicked;
		SelectionColor = Color.FromArgb(65, 65, 65);
	}

	/*private void Link_Clicked (object sender, System.Windows.Forms.LinkClickedEventArgs e)
	{
		try
		{
			System.Diagnostics.Process.Start(e.LinkText);
		}
		catch (Exception)
		{
		}
	}*/

	public void AddNames(string names)
	{
		threadSafeOutput.WriteLine(names);
		try
		{
			if (this.InvokeRequired)
			{
				AddNamesCallback d = new AddNamesCallback(AddNames);
				this.Invoke(d, new object[] { names });
			}
			else
			{
				if (names == "") 
				{
					bank.SortNames();
					Text = bank.ToString();
					return;
				}
				
				string[] split = names.Split(' ');
				foreach (string name in split)
				{
					if (name.Length == 0) continue;
					switch (name[0])
					{
						case '@': 
							bank.Add(name.Substring(1, name.Length-1), "@"); 
							break;
						case '%':
							bank.Add(name.Substring(1, name.Length-1), "%"); 
							break;
						case '+':
							bank.Add(name.Substring(1, name.Length-1), "+"); 
							break;
						default: 
							bank.Add(name); 
							break;
					}
					
				}
			}
		}
		catch (Exception e) 
		{
			threadSafeOutput.WriteLine("AddNames:");
			threadSafeOutput.WriteException(e);
		}
	}
	
	public bool ContainsName(string name)
	{
		try
		{
			if (this.InvokeRequired)
			{
				ContainsNameCallback d = new ContainsNameCallback(ContainsName);
				return (bool)this.Invoke(d, new object[] { name });
			}
			else
			{
				return bank.ContainsName(name);
			}
		}
		catch (Exception e) 
		{
			threadSafeOutput.WriteLine("ContainsName:");
			threadSafeOutput.WriteException(e);
		}
		return false;
	}
	
	public bool IsOP(string name)
	{
		try
		{
			if (this.InvokeRequired)
			{
				IsOPCallback d = new IsOPCallback(IsOP);
				return (bool)this.Invoke(d, new object[] { name });
			}
			else
			{
				return bank.IsOP(name);
			}
		}
		catch (Exception e) 
		{
			threadSafeOutput.WriteLine("IsOP:");
			threadSafeOutput.WriteException(e);
		}
		return false;
	}
	
	public void ChangeName(string oldName, string newName)
	{
		try
		{
			if (this.InvokeRequired)
			{
				ChangeNameCallback d = new ChangeNameCallback(ChangeName);
				this.Invoke(d, new object[] { oldName, newName });
			}
			else
			{
				bank.ChangeName(oldName, newName);
				bank.SortNames();
				Text = bank.ToString();
			}
		}
		catch (Exception e) 
		{
			threadSafeOutput.WriteLine("ChangeName:");
			threadSafeOutput.WriteException(e);
		}
	}

	
	public void RemoveName(string name)
	{
		try
		{
			if (this.InvokeRequired)
			{
				RemoveNameCallback d = new RemoveNameCallback(RemoveName);
				this.Invoke(d, new object[] { name });
			}
			else
			{
				bank.RemoveName(name);
				Text = bank.ToString();
			}
		}
		catch (Exception e) 
		{
			threadSafeOutput.WriteLine("RemoveName:");
			threadSafeOutput.WriteException(e);
		}
	}
}