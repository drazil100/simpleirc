using System;
using System.Collections.Generic;
using System.Text;

public class IrcMessage
{
	public string Prefix { get; set; }
	public string Command { get; set; }
	public string[] Parameters { get; set; }

	public static IrcMessage Parse(byte[] message)
	{
		return Parse(Encoding.UTF8.GetString(message));
	}

	public static IrcMessage Parse(string message)
	{
		string prefix = null;
		string command = null;
		List<string> parameters = null;

		int trailingPos = message.IndexOf(" :");
		string trailing = null;
		if (trailingPos != -1)
		{
			trailing = message.Substring(trailingPos + 2);
			message = message.Substring(0, trailingPos);
		}

		parameters = new List<string>(message.Split(' '));
		if (trailing != null)
		{
			parameters.Add(trailing);
		}

		if (parameters[0][0] == ':')
		{
			prefix = parameters[0].Substring(1);
			parameters.RemoveAt(0);
		}

		command = parameters[0];
		parameters.RemoveAt(0);

		return new IrcMessage(prefix, command, parameters.ToArray());
	}

	public IrcMessage(string command, params string[] parameters) : this(null, command, parameters)
	{
	}

	public IrcMessage(string prefix, string command, string[] parameters)
	{
		Prefix = prefix;
		Command = command.ToUpper();
		Parameters = parameters;
	}

	public override string ToString()
	{
		string output = "";

		if (Prefix != null)
			output = ":" + Prefix + " ";

		output += Command;

		for(int i = 0; i < Parameters.Length; i++) {
			if(i == Parameters.Length - 1 && Parameters[i].Contains(" "))
				output += " :" + Parameters[i];
			else
				output += " " + Parameters[i];
		}

		return output + "\r\n";
	}
}
