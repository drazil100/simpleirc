using System;
using System.Windows.Forms;
using System.Drawing;

public delegate void MessageAdderCallback(string message, string sender, Color messageColor, Color senderColor);

public class LogBox : RichTextBox
{
	public LogBox ()
	{
		ReadOnly = true;
		LinkClicked += Link_Clicked;
		//SelectionFont = new Font("Courier", 12);
		//TextChanged += Rchtxt_TextChanged;
	}

	private void Link_Clicked (object sender, System.Windows.Forms.LinkClickedEventArgs e)
	{
		try
		{
			System.Diagnostics.Process.Start(e.LinkText);
		}
		catch (Exception)
		{
		}
	}
	
	private void Rchtxt_TextChanged(object sender, EventArgs e)
        {
			int selectStart = SelectionStart;
			Select(0, TextLength);
			SelectionFont = new Font("Courier", 9);
			Select(selectStart, 0);
            //this.CheckKeyword("if", Color.Green, 0);
        }



	private void CheckKeyword(string word, Color color, int startIndex)
    {
        if (this.Text.Contains(word))
        {
            int index = -1;
            int selectStart = this.SelectionStart;

            while ((index = this.Text.IndexOf(word, (index + 1))) != -1)
            {
                this.Select((index + startIndex), word.Length);
                this.SelectionColor = color;
                this.Select(selectStart, 0);
                this.SelectionColor = Color.Black;
            }
        }
    }
	
	public void AppendText(string text, Color color)
    {
        this.SelectionStart = this.TextLength;
        this.SelectionLength = 0;

        this.SelectionColor = color;
        this.AppendText(text);
        this.SelectionColor = this.ForeColor;
    }

	public void AddMessage(string message, string sender = "")
	{
		AddMessage(message, sender, Color.Black, Color.Black);
	}
	public void AddMessage(string message, Color messageColor)
	{
		AddMessage(message, "", messageColor, Color.Black);
	}
	public void AddMessage(string message, string sender, Color messageColor, Color senderColor)
	{
		try
		{
			if (this.InvokeRequired)
			{
				MessageAdderCallback d = new MessageAdderCallback(AddMessage);
				this.Invoke(d, new object[] { message, sender, messageColor, senderColor});
			}
			else
			{
				while (Text.Length > 15000)
				{
					//  WinForms multiline textbox caps at 32KB, and UCS-2 encoding is two bytes
					//  per character, so truncate lines until we're under 16k characters.
					//  Using 16000 instead of 16384 just to leave a little bit of just-in-case wiggle room.
					SelectionStart = 0;
					SelectionLength = Text.IndexOf("\n",0) + 1;
					SelectedText = "";
				}
				if (sender != "")
				{
					Select(TextLength, TextLength);
					SelectionFont = new Font("Courier", 10, FontStyle.Bold);
					AppendText("<", Color.Black);
					SelectionFont = new Font(SelectionFont, FontStyle.Regular);
					AppendText(sender, senderColor);
					SelectionFont = new Font(SelectionFont, FontStyle.Bold);
					AppendText(">  ", Color.Black);
					SelectionFont = new Font("Courier", 9, FontStyle.Regular);
				}
				
				AppendText(message, messageColor);
				AppendText("\r\n");
				
				//string newText = Text + ((sender != "" && sender != null) ? String.Format("<{0}> {1}\r\n", sender, message) : String.Format(" {0}\r\n", message));
				//Text = newText;
				Select(TextLength, TextLength);
				ScrollToCaret();
			}
		}
		catch (Exception) {}
	}
}
