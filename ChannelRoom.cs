using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;

public class ChannelRoom : TabPage
{
	public string Channel { get; private set; }
	public string names;
	private LogBox chatLog;
	private NamesList namesList;
	private static Dictionary<string, ChannelRoom> rooms = new Dictionary<string, ChannelRoom>();
	private int namesWidth = 150;
	
	public ChannelRoom(string name, Color c)
	{
		chatLog = new LogBox();
		chatLog.BackColor = c;
		
		namesList = new NamesList();
		namesList.BackColor = c;

		Text = name;
		Channel = name;

		Controls.Add(chatLog);
		if (name.StartsWith("#"))
		{
			Controls.Add(namesList);
		}
		else
		{
			namesWidth = 0;
		}

		rooms.Add(name, this);

		Resize += delegate { DoLayout(); };

		DoLayout();
	}

	public static void SortAddMessage(string channel, string message)
	{
		SortAddMessage(channel, message, Color.Black);
	}
	public static void SortAddMessage(string channel, string message, Color messageColor)
	{
		SortAddMessage(channel, message, "", messageColor);
	}
	public static void SortAddMessage(string channel, string message, string name, Color messageColor)
	{
		Color c = Color.DarkGreen;
		if (rooms.ContainsKey(channel))
		{
			if (rooms[channel].namesList.IsOP(name))
				c = Color.DarkRed;
		}
		SortAddMessage(channel, message, name, messageColor, c);//FromArgb(0, 131, 113));
	}
	public static void SortAddMessage(string channel, string message, string name, Color messageColor, Color senderColor)
	{
		if (rooms.ContainsKey(channel))
			rooms[channel].chatLog.AddMessage(message, name, messageColor, senderColor);
	}
	
	public static void SortUpdateNames(string channel, string names)
	{
		if (rooms.ContainsKey(channel))
		{
			//rooms[channel].chatLog.AddMessage(message, name);
			rooms[channel].namesList.AddNames(names);
		}
	}
	
	public static void SortRemoveName(string channel, string name)
	{
		if (rooms.ContainsKey(channel))
		{
			//rooms[channel].chatLog.AddMessage(message, name);
			rooms[channel].namesList.RemoveName(name);
		}
	}
	
	public static void SortQuit(string name, string message)
	{
		foreach(KeyValuePair<string, ChannelRoom> room in rooms)
		{
			if (room.Value.namesList.ContainsName(name))
			{
				room.Value.chatLog.AddMessage(string.Format("* {0} has left the room: {1}", name, message), Color.DarkRed);
				room.Value.namesList.RemoveName(name);
			}
		}
	}
	
	public static void SortNickChange(string oldName, string newName)
	{
		foreach(KeyValuePair<string, ChannelRoom> room in rooms)
		{
			if (room.Value.namesList.ContainsName(oldName))
			{
				room.Value.chatLog.AddMessage(string.Format("* {0} is now known as {1}", oldName, newName, Color.Gray));
				room.Value.namesList.ChangeName(oldName, newName);
			}
			/*if (room.Value.Channel == oldName)
			{
				room.Value.changeName(newName);
			}*/
		}
	}

	public void DoLayout()
	{
		chatLog.Width = Width - namesWidth;
		chatLog.Height = Height;
		
		namesList.Left = chatLog.Width;
		namesList.Width = namesWidth;
		namesList.Height = Height;
	}

	public void Part()
	{
		rooms.Remove(Channel);
	}
}
